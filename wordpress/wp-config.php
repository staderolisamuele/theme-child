<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via web
 * puoi copiare questo file in «wp-config.php» e riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Chiavi Segrete
 * * Prefisso Tabella
 * * ABSPATH
 *
 * * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define( 'DB_NAME', 'db_prova' );

/** Nome utente del database MySQL */
define( 'DB_USER', 'root' );

/** Password del database MySQL */
define( 'DB_PASSWORD', '' );

/** Hostname MySQL  */
define( 'DB_HOST', 'localhost' );

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'uLpz)hJl-XXmXcbaJV1%$vUc<1bAeloLfu}:z74&UZ{Ww5E|2`+1pWp]l*Kv3lSe' );
define( 'SECURE_AUTH_KEY',  '*:i!;4miknuh2{i*dfYI?A$Es+V P>9$>81Q b0Cx]SREYJD!NDZ2uS|Gkf)Pwkn' );
define( 'LOGGED_IN_KEY',    'x8GP7~qN(Uq7*C@t?~Nt@_,p4m>}`7~(Y.%#Lwjr/*Sqo|zVH*O^buy;MaLNm$dc' );
define( 'NONCE_KEY',        '2LcOFWbG5kr=1`A}r>M=8[LjsWup!9 mM{v|}~%Gv_[aDK{sa}T`{zIiR~aS;;$5' );
define( 'AUTH_SALT',        '4v4CCvFmQ#wzKw:Q.nY>$yw^E{ym|,/o{8u+9o2y2k:pHuy+-1gtHhMhq9c=P{:`' );
define( 'SECURE_AUTH_SALT', 'o@o2U|&P.MU5jOId#P=2kA/QlXi68=UdtnKySd;I[d[]yj31NgV6iik+gk2*nW]q' );
define( 'LOGGED_IN_SALT',   'F&]Q4[`~l-[#}!;*[?7AlVFn>B$1fSS|uDwzU~CA(1;W@je5zWt/Qsfg5Oc]AkUE' );
define( 'NONCE_SALT',       'a5Rf,`7zpCp<%-eL:p!NUUapAqWIDdc)& -(d`}?.U[2^_G8[pz,3{1@^p:>{ju}' );

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi durante lo sviluppo
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 *
 * Per informazioni sulle altre costanti che possono essere utilizzate per il debug,
 * leggi la documentazione
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
